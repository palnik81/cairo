<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use HappyR\Google\ApiBundle\Services\YoutubeService;

class YoutubeController extends Controller
{

    private $accessScope = [
        \Google_Service_YouTube::YOUTUBE
    ];

    /**
     * @Route("/youtube/{page}", name="youtube")
     */
    public function index(Request $request, $page = null)
    {
        $session = new Session();
        if ($session->get('token'))
        {

            $token = $session->get('token');
            $client = $this->container->get('happyr.google.api.client');
            $client->setAccessToken($token);
            $client->getGoogleClient()->authorize();

            $service = new YoutubeService($client);
            $opts = array('chart' => 'mostPopular', 'maxResults' => 25);
            if (!\is_null($page))
            {
                $opts['pageToken'] = $page;
            }
            $results = $service->videos->listVideos(
                    array('part' => 'snippet'), $opts)
                    or $this->redirect('/oauth/google/auth');

            $session->set('token', $client->getGoogleClient()->getAccessToken());

            return $this->render('default/videos.html.twig', array('videos' => $results));
        } else
        {
            return $this->redirect('/oauth/google/auth');
        }
    }

}
