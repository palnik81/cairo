<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use HappyR\Google\ApiBundle\Services\YoutubeService;

class GoogleOAuthController extends Controller
{

    private $accessScope = [
        \Google_Service_YouTube::YOUTUBE
    ];

    /**
     * @Route("/oauth/google/auth")
     */
    public function getAuthenticationCodeAction()
    {
        $client = $this->container->get('happyr.google.api.client');

        // Determine the level of access your application needs
        $client->getGoogleClient()->setScopes($this->accessScope);

        // Send the user to complete their part of the OAuth
        return $this->redirect($client->createAuthUrl());
    }

    /**
     * @Route("/oauth/google/redirect")
     */
    public function getAccessCodeRedirectAction(Request $request)
    {
        $session = new Session();
            $client = $this->container->get('happyr.google.api.client');
            
        if ($request->query->get('code'))
        {
            $code = $request->query->get('code');
            $client->getGoogleClient()->setScopes($this->accessScope);
            $client->authenticate($code);

            //echo('token'.$client->getGoogleClient()->getAccessToken());
            $session->set('token', $client->getAccessToken());
            return $this->redirect('/youtube');
        } 
        elseif ($session->get('token'))
        {
            $token = $session->get('token');
            $client->getGoogleClient()->setAccessToken($token);
            $client->getGoogleClient()->authorize();
            $session->set('token', $client->getAccessToken());
            return $this->redirect('/youtube');
        } 
        else
        {
            return $this->redirect('/oauth/google/auth');
        }
    }

}
