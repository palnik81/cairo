<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Rodzic;
use AppBundle\Form\Type\DzieckoType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class RodziceDzieciController extends Controller
{

    /**
     * @Route("/rodzicedzieci")
     */
    public function indexAction(Request $request)
    {
        $rodzic = new Rodzic();
        $form = $this->createFormBuilder($rodzic)
                ->add('imie', TextType::class)
                ->add('dzieci', CollectionType::class, array(
                    'entry_type' => DzieckoType::class,
                    'allow_add' => true,
                    'label' => false,
                    'required' => false,
                    'entry_options' => array(
                        'attr' => array('class' => 'email-box'),
                    ),
                ))
                ->add('save', SubmitType::class, array('label' => 'Zapisz'))
                ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $rodzic = $form->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($rodzic);
            foreach ($rodzic->getDzieci() as $dziecko)
            {
                $dziecko->setRodzic($rodzic);
                $entityManager->persist($dziecko);
            }
            $entityManager->flush();
        }

        $list = $this->getDoctrine()->getRepository(Rodzic::class)->findAll();
        return $this->render('default/rodzicedzieci.html.twig', array(
                    'form' => $form->createView(),
                    'list' => $list,
        ));
    }

}
