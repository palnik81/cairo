<?php
namespace AppBundle\Entity;

use AppBundle\Entity\Dziecko;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 */
class Rodzic
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(type="string", length=100)
     */
    private $imie;
    
    /**
     * @ORM\OneToMany(targetEntity="Dziecko", mappedBy="rodzic", cascade={"persist"})
     */
    private $dzieci;

    public function __construct()
    {
        $this->dzieci = new ArrayCollection();
    }    

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }
    public function getImie()
    {
        return $this->imie;
    }

    public function setImie($imie)
    {
        $this->imie = $imie;
    }
    public function getDzieci()
    {
        return $this->dzieci;
    }

    public function setDzieci($dzieci)
    {
        $this->dzieci = $dzieci;
    }
    public function addDziecko(Dziecko $dziecko)
    {
        $dziecko->setRodzic($this);
        $this->dzieci->add($dziecko);
    }        
}