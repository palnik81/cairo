<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Rodzic;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Dziecko {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Rodzic", inversedBy="dzieci")
     * @ORM\JoinColumn(name="rodzic_id", referencedColumnName="id")
     */
    private $rodzic;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $imie;

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getImie() {
        return $this->imie;
    }

    public function setImie($imie) {
        $this->imie = $imie;
    }

    public function getRodzic() {
        return $this->rodzic;
    }

    public function setRodzic(Rodzic $rodzic) {
        $this->rodzic = $rodzic;
    }

}
